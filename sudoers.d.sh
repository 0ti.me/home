#!/usr/bin/env bash

main() {
  local BASENAME=
  local CODE=
  local DEST=
  local GRP=root
  local RESULT=
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"
  local TMP=

  . "${THIS_DIR}/.bashrc.d/os"

  while read EACH; do
    BASENAME="$(basename "${EACH}")"
    DEST="/etc/sudoers.d/${BASENAME}"
    TMP="${EACH}.tmp"

    RESULT="$(visudo -c "${EACH}" 2>&1 || >&2 echo -ne)"
    CODE=$?

    # Check each with visudo even if we're not applying anything
    if [ ${CODE} -ne 0 ]; then
      >&2 echo "${RESULT}"

      return ${CODE}
    fi

    if macos; then
      GRP=wheel
    fi

    if ! [ -f "${DEST}" ] || >/dev/null 2>&1 diff "${EACH}" "${DEST}"; then
      sudo cp "${EACH}" "${TMP}" \
        && sudo chown root:${GRP} "${TMP}" \
        && sudo chmod 440 "${TMP}" \
        && sudo mv "${TMP}" "${DEST}"
    fi
  done < <(find "${THIS_DIR}/sudoers.d" -type f)
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  main "$@"
fi
