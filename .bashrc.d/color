#!/usr/bin/env bash

color() {
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"

  . "${THIS_DIR}/colors_aa"
  . "${THIS_DIR}/os"

  local ARGS=()
  local ANNOUNCE_COLOR="${ANNOUNCE_COLOR-light_blue}"
  local ZERO_COLOR="${ZERO_COLOR-green}"
  local NONZERO_COLOR="${NONZERO_COLOR-red}"
  local CODE=
  local ELAPSED_TIME=
  local END_TIME=
  local FACTOR=
  local NEWLINE=0
  local START_TIME=
  local TIME_UNITS=s

  format_date() {
    TIME_UNITS=s
    echo "$1 / ${FACTOR}" | bc -l | awk '{printf "%.3f", $0}'
    # printf '%.3f' $(echo "$1 / ${FACTOR}" | bc -l)
  }

  if macos; then
    if which gdate >/dev/null 2>&1; then
      color_time() {
        gdate +%s%N
      }
      FACTOR=1000000000
    else
      color_time() {
        date +%s
      }
      FACTOR=1
    fi
  elif linux; then
    color_time() {
      date +%s%N
    }
    FACTOR=1000000000
  else
    >&2 echo "OS not supported"
    return -1
  fi

  START_TIME=$(color_time)

  # >&2 echo_in_color ${ANNOUNCE_COLOR} "+ $@\n"

  while [ $# -gt 0 ]; do
    case $1 in
      -n|--newline)
        NEWLINE=1
        ;;
      --)
        shift
        ARGS=("$@")
        break
        ;;
      *)
        ARGS+=("$1")
        ;;
    esac

    shift
  done

  # >&2 echo "+$(printf " %q" "${ARGS[@]}")"
  ("${ARGS[@]}"); CODE=$?

  END_TIME=$(color_time)
  ELAPSED_TIME=$(format_date $(echo "${END_TIME} - ${START_TIME}" | bc -l))

  if [ ${NEWLINE} -eq 1 ]; then
    >&2 echo
  fi

  if [ ${CODE} -eq 0 ]; then
    >&2 echo_in_color ${ZERO_COLOR} + "${ARGS[@]}" Success after ${ELAPSED_TIME}${TIME_UNITS} "(code: ${CODE})\n"
  else
    >&2 echo_in_color ${NONZERO_COLOR} + "${ARGS[@]}" Fail after ${ELAPSED_TIME}${TIME_UNITS} "(code: ${CODE})\n"
  fi

  return ${CODE}
}

if [ -n "$ZSH_VERSION" ] && ! [[ $ZSH_EVAL_CONTEXT =~ :file$ ]]; then
  color "$@"
fi
