#!/usr/bin/env bash

main() {
  local MODESET_DIR=/etc/modprobe.d/
  local MODESET_CONF="${MODESET_DIR}/nvidia-drm-nomodeset.conf"

  if [ -e /dev/nvidia-modeset ]; then
    if [ ! -f ${MODESET_CONF} ] \
      && ! grep -rE "options nvidia-drm modeset=1" "${MODESET_DIR}" >/dev/null 2>&1; then
      echo "options nvidia-drm modeset=1" | sudo tee ${MODESET_CONF}

      sudo update-initramfs -u -k all
    fi
  fi
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  main "$@"
fi
