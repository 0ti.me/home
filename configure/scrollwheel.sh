#!/usr/bin/env bash

main() {
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"
  local BIN_DIR="$(cd "${THIS_DIR}/../bin" && pwd)"
  local BASHRCD_DIR="$(cd "${THIS_DIR}/../.bashrc.d" && pwd)"

  . "${BASHRCD_DIR}/os"

  if macos; then
    defaults write .GlobalPreferences com.apple.scrollwheel.scaling -1
  fi
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  main "$@"
fi
