// order acquired here: https://www.computerhope.com/unix/signals.htm
const signals = [
  'SIGHUP',
  'SIGINT',
  'SIGQUIT',
  'SIGILL',
  'SIGTRAP',
  'SIGABRT',
  'SIGBUS',
  'SIGFPE',
  'SIGKILL',
  'SIGUSR1',
  'SIGSEGV',
  'SIGUSR2',
  'SIGPIPE',
  'SIGALRM',
  'SIGTERM',
  'SIGSTKFLT',
  'SIGCHLD',
  'SIGCONT',
  'SIGSTOP',
  'SIGTSTP',
  'SIGTTIN',
  'SIGTTOU',
  'SIGURG',
  'SIGXCPU',
  'SIGXFSZ',
  'SIGVTALRM',
  'SIGPROF',
  'SIGWINCH',
  'SIGIO',
  'SIGPWR',
  'SIGUNUSED',
];

const get = (numOrName, defaultSignal = undefined) => {
  let number, name;

  const type = typeof numOrName;

  if (type === 'number') {
    number = numOrName;
  } else if (type === 'string') {
    if (/^[0-9]+$/.test(numOrName)) {
      number = parseInt(numOrName, 10);
    } else {
      name = numOrName;
    }
  } else if (defaultSignal !== undefined) {
    return get(defaultSignal);
  }

  if (number !== undefined) {
    name = signals[number - 1];
    number = number;
  } else if (name !== undefined) {
    for (let i = 0; i < signals.length; ++i) {
      if (signals[i] === name.toUpperCase()) {
        name = signals[i];
        number = i + 1;
      }
    }
  }

  if (!name || number === undefined) {
    throw new Error(`unable to decode signal from ${numOrName}`);
  }

  return { name, number };
};

module.exports = signals;
module.exports.default = signals;
module.exports.get = get;
