// returns false for any falsy value, '0', or 'false'

const expandedFalsy = (x) => x && x !== '0' && x !== 'false';

module.exports = expandedFalsy;
module.exports.default = expandedFalsy;
