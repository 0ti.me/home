const crypto = require('crypto');
const fs = require('fs');
const fsP = require('fs/promises');
const path = require('path');

const find = require('../path/find.cjs');
const spawnPromise = require('../spawn-promise.cjs');

function chck(hashName, path) {
  return new Promise((resolve, reject) => {
    const hash = crypto.createHash(hashName);
    const stream = fs.createReadStream(path);
    stream.on('error', (err) => reject(err));
    stream.on('data', (chunk) => hash.update(chunk));
    stream.on('end', () => resolve(hash.digest('hex')));
  });
}

const mainBackupPath = path.resolve(
  process.env.HOME,
  '.config',
  'hank',
  'fix-notification-sounds',
);

const configureAndFixNotificationSounds = async (options) => {
  const { paths, replacements, reductionHardcodes } = Object.assign(
    {
      // defaults
      reductionHardcodes: { 'example.mp3': '0.2' },
      replacements: {},
    },
    options,
  );

  if (!paths || !Array.isArray(paths) || paths.length < 1)
    throw new Error(`invalid config ${JSON.stringify(options)}`);

  const files = await find(paths, {
    name: ['*.mp3', '*.wav'],
    type: 'f',
  });

  const backupPath = path.resolve(mainBackupPath, 'slack');

  await fsP.mkdir(backupPath, { recursive: true });

  for (const file of files) {
    const target = file.fullpath;
    const basename = path.basename(target);
    const replacement = replacements[basename];
    const backupFile = path.resolve(backupPath, basename);

    if (replacement) {
      const [current, desired, backup] = await Promise.all([
        chck('sha256', target),
        chck('sha256', path.resolve(replacement)),
        chck('sha256', backupFile).catch(() => {}), // ignore errors, it's not there probably
      ]);

      if (current !== desired) {
        if (current !== backup) {
          console.error(42, `mv ${target} ${backupFile}`);
          fsP.rename(target, backupFile);
        }

        console.error(46, `cp ${replacement} ${target}`);
        fsP.copyFile(replacement, target);
      }

      console.info({ fullpath: target, backup, current, desired });
    } else {
      const reducedPathHashFile = `${backupFile}.reduced.sha256`;

      const [current, backup, reducedPathHash] = await Promise.all([
        chck('sha256', target),
        chck('sha256', backupFile).catch(() => {}), // ignore errors, it's not there probably
        fsP.readFile(reducedPathHashFile, 'utf-8').catch(() => {}), // ignore errors, it's not there probably
      ]);

      if (current !== reducedPathHash) {
        if (current !== backup) {
          console.error(74, `mv ${target} ${backupFile}`);
          await fsP.rename(target, backupFile);
        } else {
          await fsP.rm(target);
        }

        const volume = reductionHardcodes[basename] ?? '0.5';

        console.error(
          79,
          `ffmpeg -i ${backupFile} -af volume=${volume} ${target}`,
        );

        await spawnPromise('ffmpeg', [
          '-nostdin',
          '-i',
          backupFile,
          '-af',
          `volume=${volume}`,
          target,
        ]);

        await fsP.writeFile(
          reducedPathHashFile,
          await chck('sha256', target),
          'utf-8',
        );
      }
    }
  }
};

const fixSlackNotificationSounds = async () => {
  return configureAndFixNotificationSounds({
    paths: ['/Applications/Slack.app'],
    replacements: { 'hummus.mp3': 'sounds/Blip.ogg' },
  });
};

const fixOutlookNotificationSounds = async () =>
  configureAndFixNotificationSounds({
    paths: ['/Applications/Microsoft Outlook.app'],
  });

const fixNotificationSounds = async () => {
  await Promise.all([
    fixSlackNotificationSounds(),
    fixOutlookNotificationSounds(),
  ]);
};

module.exports = fixNotificationSounds;
module.exports.fixNotificationSounds = fixNotificationSounds;

if (require.main === module) {
  fixNotificationSounds()
    .then(() => process.exit(0))
    .catch((err) => {
      console.error(err.stack);

      process.exit(1);
    });
}
