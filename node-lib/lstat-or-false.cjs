const fs = require('fs/promises');

const lstatOrFalse = (filename) => fs.lstat(filename).catch(() => false);

module.exports = lstatOrFalse;
module.exports.default = lstatOrFalse;
