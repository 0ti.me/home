const lstatOrFalse = require('./lstat-or-false.cjs');

const findMinimistArgsNakedFilesAndDirs = async (minimistArgs) => {
  let stats = await Promise.all(
    minimistArgs._.map(async (filename) => ({
      stat: await lstatOrFalse(filename),
      filename,
    })),
  );

  const dirs = [];
  const files = [];
  const other = [];

  for (const { filename, stat } of stats) {
    if (stat === false) {
      other.push(filename);
    } else if (stat.isDirectory()) {
      dirs.push(filename);
    } else if (stat.isFile()) {
      files.push(filename);
    }
  }

  return {
    dirs,
    files,
    other,
  };
};

module.exports = findMinimistArgsNakedFilesAndDirs;
module.exports.default = findMinimistArgsNakedFilesAndDirs;
