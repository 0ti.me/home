const deadChildren = 11000;

const daysOfGenocide =
  1 + Math.floor((new Date() - new Date(2023, 9, 7)) / (3600 * 24000));

module.exports = {
  lists: {
    palestine: [
      `Day ${daysOfGenocide}: Genocide Joe is enabling genocide in Gaza. Dems and Repubs in Congress are also complicit.`,
      'Palestine tried peaceful protest in 2018, and the world ignored them. Watch Gaza Fights for Freedom.',
      'Israeli PM David Ben-Gurion: "I would never make terms with Israel...we have taken their country"',
      'David Ben-Gurion: "...politically we are the aggressors and they defend themselves...The country is theirs..."',
      'The org Jewish Voices for Peace (JVP) is full of prominent Jewish voices in favor of Palestinian human rights.',
      'The US has given Israel at least 3 billion dollars per year every year since 1976. How many bridges, roads, hospitals, and schools could have been built instead? How many jobs could have been created with that money?',
      'An estimated 186,000 Gazan civilians have been murdered by so-called Israel since October 7th. Ceasefire NOW!',
      "It's not war, it's a genocide. From the river to the sea, Palestine will be free! Watch Salt of this Sea on Nflx.",
      'The Electronic Intifada, meaning Electronic Uprising, is a great source of Palestinian voices and info!',
      "So-called Israel's genocide and ethnic cleansing started even before 1948 and continue today. 75 years of occupation.",
      `1.7 million Gazan civilians have been displaced by so-called Israel's actions over these ${daysOfGenocide} days of genocide.`,
      'So-called Israel is obligated under intl law to protect civilians - they intentionally kill civilians and journalists.',
      'The IOF claimed they had killed 60 Hamas operatives out of the then 10,000 dead. That is a 99.4% civilian death toll.',
      'ICJ found sufficient evidence to investigate so-called Israel for the crime of genocide against the Palestinian people.',
      'In order to show your solidarity with Gaza, call your political leaders to demand a ceasefire now!',
    ],
  },
  palestine: {
    antiSemitism:
      'Conflating zionism or the apartheid so-called State of Israel with jews in general is itself anti-semitic.',
    babies: `How many babies need to die before you start to care? ${deadChildren} dead children isn't enough for you to care?`,
    broodWar: 'This is Starcraft: Brood War!',
    doNotCare:
      "Even if it's uncomfortable, caring about genocide is crucial for our humanity. Throughout history, movements led by individuals have reshaped societies. Your involvement can impact justice and prevent future suffering.",
    doNotCareCoward:
      "Oh, for sure, I get it. Caring takes courage and you're a coward.",
    doNotCareSoMuch:
      "Dude, I don't care so much about what you're saying that I am compelled to respond to you to tell you that I don't care. I promise I don't care so much. I've never cared less about anything in my life.",
    doNotCareWhyRespond:
      "Makes sense that you would respond to something you don't care about rather than just be silent or mute me. It's almost like you do actually care, but you don't want me to talk about it. I bet you can guess how successful your approach is going to be in making me stop.",
    electedHamas: `Hamas was elected in 2006. No one under let\'s say 30 in Gaza today participated in that election. Who exactly do you think voted for Hamas among the ${deadChildren} dead children?`,
    hamas:
      'Someone broke into my house and was holding me at gunpoint in the basement until the police arrived and negotiated a two-house solution wherein I get to keep the basement. That is the reality of the two-state solution.',
    hopeForYou:
      'I hope when you are the victim of violence like this probably from climate change that people rally to your defense',
    oldDoNotCare:
      "Remember 1940 Germany? When we witness an injustice and don't act, we train our character to be passive in its presence.",
    oneState:
      "Israel's leaders want one apartheid State excluding Palestinians, Palestine's - a free, fair, and secular State for all.",
    oppressors:
      'In the western psyche, the Israelis can never be seen to be committing crimes as severe as the germans.',
    protestPurpose:
      "The point of protest is inconvenience, so your reaction proves it's working. You're clearly uncomfortable.",
    religiousConflict:
      'This is not rooted in religious conflict. Israel slaughters Palestinian Jews and Christians, too.',
    shouldNotHaveAttackedOctober:
      'If I broke into your house, held you at gunpoint, waited for the cops to show up to negotiate a solution where I get your whole house except your basement and your attic, would you tolerate that or would you fight me to get your house back?',
    riverToTheSea:
      'Every accusasion is a confession, when Netanyahu says from the river to the sea he means extermination of Arabs. Palestinians mean one secular state free for all.',
    shutup:
      'Genocide demands voices, not silence. We must confront injustice and advocate for those who cannot speak for themselves.',
    thankYouForSubscribing:
      'Thank you for subscribing to Palestine facts. You can only unsubscribe by observing the end of the genocide.',
    trembleIndignation:
      'If you tremble with indignation at every injustice then you are a comrade of mine.',
    whatCanIDo:
      'Keeping Gaza in the headlines is one of the main things we can do in the imperial core. Go to protests, organize locally, call your representatives, and above all, do not stop talking about Gaza. All change in human history derives from mass movements.',
    wrongTimeOrPlace:
      "Human suffering knows no bounds. Genocide anywhere demands attention everywhere. Gaza's plight cannot be silenced by trivialities.",
  },
};

if (require.main === module) {
  console.log(JSON.stringify(module.exports));
}
