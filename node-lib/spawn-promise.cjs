const { spawn } = require('child_process');

const spawnPromise = (...args) => {
  if (args.length > 1 && Array.isArray(args[1])) {
    console.error('+', args[0], args[1].join(' '));
  } else {
    console.error('+', args[0]);
  }

  const proc = spawn(...args);

  const promise = new Promise((resolve, reject) => {
    let fulfilled = false;

    proc.on('error', (err) => fulfilled || reject((fulfilled = true) && err));

    proc.on(
      'close',
      (code) => fulfilled || resolve((fulfilled = true) && code),
    );
  });

  promise._spawnPromiseProc = proc;

  promise.stdout = {
    on: (...args) => {
      proc.stdout.on(...args);

      return promise;
    },
    pipe: (...args) => {
      proc.stdout.pipe(...args);

      return promise;
    },
  };

  promise.stderr = {
    on: (...args) => {
      proc.stdout.on(...args);

      return promise;
    },
    pipe: (...args) => {
      proc.stderr.pipe(...args);

      return promise;
    },
  };

  promise.pipeToConsole = () => {
    proc.stdout.pipe(process.stdout);
    proc.stderr.pipe(process.stderr);

    return promise;
  };

  promise.pipeStderrToConsole = () => {
    proc.stderr.pipe(process.stderr);

    return promise;
  };

  promise.pipeStdoutToConsole = () => {
    proc.stdout.pipe(process.stdout);

    return promise;
  };

  return promise;
};

module.exports = spawnPromise;
module.exports.default = spawnPromise;
