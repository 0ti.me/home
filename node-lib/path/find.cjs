const fs = require('fs/promises');
const { minimatch } = require('minimatch');
const minimist = require('minimist');
const path = require('path');

const FindType = {
  ALL: 0,
  FILE: 1,
  DIR: 2,
};

const addIsFile = async (info) => {
  const stat = await fs.stat(info.fullpath);

  return {
    ...info,
    isDir: stat.isDirectory(),
    isFile: stat.isFile(),
  };
};

const find = async (initialTargets = [], options = {}) => {
  let toFind = FindType.ALL;

  switch (options.type) {
    case 'f':
      toFind = FindType.FILE;
      break;
    case 'd':
      toFind = FindType.DIR;
      break;
    case undefined:
      break;
    default:
      throw new Error(`unsupported type ${options.type}`);
  }

  const shouldMarkFound = (file, debug = false) => {
    let markFound = true;

    switch (options.type) {
      case FindType.ALL:
        break;
      case FindType.FILE:
        if (!file.isFile) markFound = false;
        break;
      case FindType.DIR:
        if (file.isFile) markFound = false;
        break;
    }

    markFound &&= !!(
      Array.isArray(options.name) ? options.name : [options.name]
    ).find((name) => {
      if (name) {
        const basename = path.basename(file.fullpath);

        if (debug) {
          console.info(
            52,
            markFound,
            basename,
            `minimatch(${basename}, ${name}) === ${minimatch(basename, name)}`,
          );
        }

        return minimatch(basename, name);
      }
    });

    return markFound;
  };

  const mappedTargets = await Promise.all(
    initialTargets.map((fullpath) =>
      addIsFile({
        basename: path.basename(fullpath),
        dirname: path.dirname(fullpath),
        fullpath,
      }),
    ),
  );

  const dirs = mappedTargets.filter((ea) => !ea.isFile);
  const found = mappedTargets.filter(shouldMarkFound);

  while (dirs.length) {
    const dir = dirs.shift();
    let dirContents;

    try {
      dirContents = await Promise.all(
        (await fs.readdir(dir.fullpath)).map(async (result) => {
          const basename = path.join(dir.basename, result);
          const fullpath = path.join(dir.dirname, basename);

          return addIsFile({
            basename,
            dirname: dir.dirname,
            fullpath,
          });
        }),
      );
    } catch (err) {
      if (err.code === 'EACCES') {
        if (options.verbose) {
          console.error(
            `Ignoring permissions error accessing file ${dir.fullpath}`,
          );
        }
        dirContents = [];
      } else {
        throw err;
      }
    }

    for (const ea of dirContents) {
      if (ea.isDir) dirs.push(ea);

      // isDebug = path.basename(ea.fullpath) === 'swapmouse.vim';
      const markingFound = shouldMarkFound(ea /* isDebug */);

      /*
      if (isDebug) {
        console.info(ea, markingFound, shouldMarkFound(ea), options);
      }
      */

      if (markingFound) {
        found.push(ea);
      }
    }
  }

  return found;
};

module.exports = find;
module.exports.find = find;

if (require.main === module) {
  const m = minimist(process.argv.slice(2));
  find(m._, m)
    .then((data) => {
      console.log(data.map(({ fullpath }) => fullpath).join('\n'));
      process.exit(0);
    })
    .catch((err) => {
      console.error(err);

      process.exit(1);
    });
}
