const psListPromise = import('ps-list');

const ps = async (options) => {
  const { exclude, filters, format } = options ?? {};
  let { regex } = filters ?? {};
  const { str } = filters ?? {};

  if (typeof regex === 'string') regex = new RegExp(regex);

  const { default: psList } = await psListPromise;

  const list = await psList();

  const result = [];

  for (let i = 0; i < list.length; ++i) {
    const eachProcess = list[i];
    const { cmd } = eachProcess;

    let match =
      regex?.find((r) => !!r.exec(cmd)) || str?.find((s) => cmd.includes(s));

    if (match && !(exclude && exclude(eachProcess, i, result))) {
      result.push(format ? format(eachProcess, i, result) : eachProcess);
    }
  }

  return result;
};

module.exports = ps;
module.exports.default = ps;
