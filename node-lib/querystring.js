const querystringRegex = /^[^?]+[?]([^#]+).*$/;

const getHref = () => document && document.location && document.location.href;

module.exports = (source = getHref()) => {
  const [, result] = querystringRegex.exec(source);

  return result.split('&').reduce((acc, ea) => {
    let [key, value] = ea.split('=');

    value = decodeURIComponent(value);

    if (Array.isArray(acc[key])) {
      acc[key].push(value);
    } else if (acc[key]) {
      acc[key] = [acc[key], value];
    } else {
      acc[key] = value;
    }

    return acc;
  }, {});
};

module.exports.get = (key, options = {}) => {
  const firstOnly = options.firstOnly === true;
  const value = module.exports(options.url)[key];

  if (firstOnly && Array.isArray(value)) {
    return value[0];
  } else {
    return value;
  }
};

module.exports.toQueryString = (
  obj,
  options = {
    encode: true,
  },
) =>
  Object.keys(obj).reduce((acc, key) => {
    const val = options.encode ? encodeURIComponent(obj[key]) : obj[key];
    let result = acc.length > 0 ? `${acc}&` : acc;

    if (Array.isArray(val)) {
      for (let i = 0; i < val.length; ++i) {
        if (i === 0) {
          result += `${key}=${val[i]}`;
        } else {
          result += `&${key}=${val[i]}`;
        }
      }
    } else {
      result += `${key}=${val}`;
    }

    return result;
  }, '');
