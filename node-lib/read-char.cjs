module.exports = async (prompt = '') => {
  let error;
  let errorHandler;
  let characterRead;
  let readableHandler;

  try {
    characterRead = await new Promise((resolve, reject) => {
      errorHandler = reject;
      readableHandler = () => resolve(process.stdin.read().toString()[0]);

      if (prompt) {
        process.stderr.write(prompt);
      }

      process.stdin.on('error', errorHandler);
      process.stdin.on('readable', readableHandler);
    });
  } catch (err) {
    error = err;
  }

  if (errorHandler) {
    process.stdin.removeListener('error', errorHandler);
  }

  if (readableHandler) {
    process.stdin.removeListener('readable', readableHandler);
  }

  if (error) throw error;

  return characterRead;
};
