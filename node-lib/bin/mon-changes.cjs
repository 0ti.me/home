#!/usr/bin/env node

const fs = require('fs');
const minimist = require('minimist');

const expandedFalsy = require('../expanded-falsy.cjs');
const findMinimistNakedFilesAndDirs = require('../find-minimist-naked-files-and-dirs.cjs');
const allSignals = require('../signals/list.cjs');
const spawnPromise = require('../spawn-promise.cjs');

const excludedFilters = ['SIGKILL', 'SIGCHLD', 'SIGSTOP'];

const signals = allSignals.filter((x) => !excludedFilters.includes(x));

// TODO: Make a real logger
const logFn =
  (l) =>
  (...args) =>
    console.error(`${l} ${new Date().toISOString()}:`, ...args);

const logger = {
  debug: logFn('D'),
  error: logFn('E'),
  info: logFn('I'),
  // for streaming through to other processes
  log: console.log,
  warn: logFn('W'),
};

const monChanges = async (args) => {
  let commandExecCount = 0;
  const { dirs, files, other } = await findMinimistNakedFilesAndDirs(args);

  // initialize c to something truthy
  let c = args.c ? args.c : [];
  // ensure it's an array
  c = Array.isArray(c) ? c : [c];

  const commands = [...other, ...c].reduce((acc, commandString) => {
    // TODO: Space separated value parsing
    const [command, ...args] = commandString.split(' ');

    logger.debug(commandString, command, args);

    acc.push([command, args]);

    return acc;
  }, []);

  const watchers = [];

  const close = (something) => {
    if (close._executed) {
      return;
    }

    close._executed = true;

    logger.debug(something, 'Closing all handlers');

    watchers.forEach((watcher) => watcher.close());

    logger.debug('Closed all handlers');
  };

  const handler = async (eventname, filename) => {
    logger.debug(`eventname: "${eventname}" - filename: "${filename}" changed`);

    let result = [];

    ++commandExecCount;

    if (expandedFalsy(args.max) && commandExecCount >= args.max) {
      close();
    }

    if (expandedFalsy(args.parallel)) {
      result = await Promise.all(
        commands.map(([command, args]) => {
          if (expandedFalsy(args.includeFilename)) {
            args.push(filename);
          }

          return spawnPromise(command, args);
        }),
      );
    } else {
      for (const [command, args] of commands) {
        logger.debug(
          `running command "${command}" with args "${JSON.stringify(args)}"`,
        );

        if (expandedFalsy(args.includeFilename)) {
          args.push(filename);
        }

        result.push(await spawnPromise(command, args));
      }
    }

    return result;
  };

  const errorHandler = (err) => {
    logger.error(err);
  };

  let watcher;

  // before creating any watchers, set up signal handlers to prevent closing without
  // stopping watchers
  signals.forEach((signal) => {
    logger.debug(`process.on(${signal}) binding...`);
    process.on(signal, close);
  });

  dirs.forEach((target) => {
    // abort if already closed for some reason
    if (close._executed) return;

    logger.debug(`Creating watcher for directory ${target}`);
    watcher = fs.watch(target, { persistent: true, recursive: true }, handler);
    watcher.on('error', errorHandler);
    watchers.push(watcher);
  });

  files.forEach((target) => {
    // abort if already closed for some reason
    if (close._executed) return;

    logger.debug(`Creating watcher for file ${target}`);
    watcher = fs.watch(target, { persistent: true }, handler);
    watcher.on('error', errorHandler);
    watchers.push(watcher);
  });

  return new Promise((resolve) => {
    let interval = setInterval(() => {
      if (close._executed) {
        clearInterval(interval);
        resolve();
      }
    }, 100);
  });
};

module.exports = monChanges;
module.exports.default = monChanges;

if (require.main === module) {
  monChanges(minimist(process.argv.slice(2))).catch((err) => {
    logger.error(err);
    return process.exit(1);
  });
}
