#!/usr/bin/env node

const from = Buffer.from;

const bufferFromBase64 = (y) =>
  from(y, from([0x62, 0x61, 0x73, 0x65, 0x36, 0x34]).toString()).toString();

const floor = Math[bufferFromBase64('Zmxvb3I=')];
const random = Math[bufferFromBase64('cmFuZG9t')];
const robotJs = require(bufferFromBase64('cm9ib3Rqcw=='));
const getScreenSize = robotJs[bufferFromBase64('Z2V0U2NyZWVuU2l6ZQ==')];
const nextInt = (maximumValue, minimumValue) =>
  floor(random() * (maximumValue - minimumValue) + minimumValue);
const moveMouseSmooth = (...x) =>
  robotJs[bufferFromBase64('bW92ZU1vdXNlU21vb3Ro')](...x);
const getMousePos = () => robotJs[bufferFromBase64('Z2V0TW91c2VQb3M')]();
const objectValues = (x) => Object[bufferFromBase64('dmFsdWVz')](x);

const widthString = bufferFromBase64('d2lkdGg=');
const heightString = bufferFromBase64('aGVpZ2h0');

const g = global;
const globalSetTimeout = g[bufferFromBase64('c2V0VGltZW91dA==')];
const globalSetInterval = g[bufferFromBase64('c2V0SW50ZXJ2YWw=')];

// robotJs set mouse delay 2
robotJs[bufferFromBase64('c2V0TW91c2VEZWxheQ==')](2);

const getEnvOrDefault = (x, d) => process[bufferFromBase64('ZW52')][x] || d;

const maxTime = getEnvOrDefault('rx', 37500);
const minTime = getEnvOrDefault('rm', 7500);

const stimulation = 5;
const delay = () => nextInt(maxTime, minTime);
const dateNow = () => Date[bufferFromBase64('bm93')]();

const eventually = (fn, timeout = delay()) => globalSetTimeout(fn, timeout);
const regularly = (fn, timeout = delay()) => globalSetInterval(fn, timeout);

let previousMousePos = getMousePos();
let lastDetectedTimeChange = 0;

const checkForMouseMovement = (onChangeCb) => {
  const currentMousePos = getMousePos();

  let changed = false;

  const [[prevX, prevY], [currX, currY]] = [
    objectValues(previousMousePos),
    objectValues(currentMousePos),
  ];

  if (prevX !== currX || prevY !== currY) {
    lastDetectedTimeChange = dateNow();

    changed = true;

    onChangeCb && onChangeCb();
  }

  previousMousePos = currentMousePos;

  console.error('was there new mouse movement?', changed);

  return changed;
};

regularly(checkForMouseMovement, 20000);

const timeDiffThreshold = 30000;

const jiggle = () => {
  const ss = getScreenSize();
  const halfHeight = ss[heightString] / 2.0;
  const halfWidth = ss[widthString] / 2.0;
  const now = dateNow();

  const timeUp = now > lastDetectedTimeChange + timeDiffThreshold;
  // prettier-ignore
  console.error(`${now} > ${lastDetectedTimeChange} + ${timeDiffThreshold}: ${timeUp}`);
  console.error('now > lastDetectedTimeChange + timeDiffThreshold?', timeUp);

  if (timeUp && !checkForMouseMovement()) {
    const pos = getMousePos();
    const horizontalPosition = pos[bufferFromBase64('eA==')];
    const verticalPosition = pos[bufferFromBase64('eQ==')];

    let minX;
    let maxX;
    let minY;
    let maxY;

    if (horizontalPosition >= halfWidth) {
      minX = stimulation - 1;
      maxX = 0;
    } else {
      minX = 1;
      maxX = stimulation;
    }

    if (verticalPosition >= halfHeight) {
      minY = stimulation - 1;
      maxY = 0;
    } else {
      minY = 1;
      maxY = stimulation;
    }

    const [horizontalChange, verticalChange] = [
      nextInt(maxX, minX),
      nextInt(maxY, minY),
    ];

    // prettier-ignore
    console.error(`stimulating mouse movement (${horizontalPosition}, ${verticalPosition}) + (${horizontalChange}, ${verticalChange})`);

    moveMouseSmooth(
      horizontalPosition - horizontalChange,
      verticalPosition - verticalChange,
    );

    // update current mouse position
    checkForMouseMovement();
  }

  eventually(jiggle);
};

jiggle();
