#!/usr/bin/env node

const cliui = require('cliui');
const debug = require('debug')('kg');
const minimist = require('minimist');
const ps = require('../ps.cjs');
const readChar = require('../read-char.cjs');
const { default: signals, get } = require('../signals/list.cjs');

const regexWithOptionsParser = /^\/(.*)\/([dgimsuy]*)$/;

class InternalErrorCompilingRegexes extends Error {}

const minimistOpts = signals.reduce(
  (acc, _, i) => {
    acc.boolean.push((i + 2).toString());

    return acc;
  },
  { boolean: ['i'], default: { i: false } },
);

const compileRegex = (r, regexGlobalDefaultOptions) => {
  try {
    const capturedPotentialRegex =
      regexWithOptionsParser.exec(r)?.slice(1) ?? [];

    let regex;

    debug('captured potential regex', capturedPotentialRegex);

    if (capturedPotentialRegex.length > 2) {
      const err = new InternalErrorCompilingRegexes(
        `hopefully this cannot happen capturedPotentialRegex.length > 2 from string "${r}"`,
      );

      throw err;
    } else if (capturedPotentialRegex.length > 0) {
      let stringRegex = capturedPotentialRegex[0];
      const stringifiedDedupedOptions = Object.keys(
        regexGlobalDefaultOptions ?? {},
      )
        .concat(capturedPotentialRegex[1].split(''))
        .sort()
        .filter((each, i, list) => i === 0 || list[i - 1] != each)
        .join('');

      regex = new RegExp(stringRegex, stringifiedDedupedOptions);

      debug(
        `created regex from string "${capturedPotentialRegex[0]}" with options "${capturedPotentialRegex[1]}"`,
      );
    } else {
      const stringifiedRegexGlobalDefaultOptions = Object.keys(
        regexGlobalDefaultOptions ?? {},
      ).join('');

      regex = new RegExp(r, stringifiedRegexGlobalDefaultOptions);

      debug(
        `created regex from string "${r}" with options "${stringifiedRegexGlobalDefaultOptions}"`,
      );
    }

    return regex;
  } catch (err) {
    if (err instanceof InternalErrorCompilingRegexes) {
      throw err;
    }

    debug('silencing error', err);

    return null;
  }
};

const ensureArray = (pos) => (pos ? (Array.isArray(pos) ? pos : [pos]) : []);

const kg = async (args) => {
  const regex = [];
  const str = [];

  for (const each of args._) {
    const r = compileRegex(each, { i: args.i });

    if (r) {
      regex.push(r);
    } else {
      str.push(each);
    }
  }

  for (const each of ensureArray(args.regex)) {
    regex.push(each);
  }

  for (const each of ensureArray(args.string)) {
    str.push(each);
  }

  let longestFields = {
    cmd: 0,
    name: 0,
    pid: 0,
  };

  const ui = cliui();

  const list = await ps({
    exclude: ({ pid }) => pid === process.pid,
    filters: {
      regex,
      str,
    },
    format: (each) => {
      longestFields.cmd = Math.max(longestFields.cmd, each.cmd.length);
      longestFields.name = Math.max(longestFields.name, each.name.length);
      longestFields.pid = Math.max(
        longestFields.pid,
        each.pid.toString().length,
      );

      return each;
    },
  });

  if (!list?.length) {
    console.error('no processes matching filters found');

    return;
  }

  const widthObj = {
    name: Math.min(longestFields.name ?? 12, 12) + 3,
    cmd: Math.min(longestFields.cmd ?? 97, 97) + 3,
    pid: Math.min(longestFields.pid ?? 7, 7) + 3,
  };

  const fields = ['name', 'pid', 'cmd'];
  const widths = fields.map((key) => widthObj[key]);

  ui.div(...fields.map((text, i) => ({ text, width: widths[i] })));

  for (const each of list) {
    ui.div(...fields.map((key, i) => ({ text: each[key], width: widths[i] })));
  }

  console.error(ui.toString());

  const numericStringRegex = /^[0-9]+$/;

  const findDo = (list, operation) => {
    const found = list.findIndex((x) => operation(x) !== undefined);

    // mirror Array.find functionality
    return found >= 0 ? operation(list[found]) : undefined;
  };

  const signal =
    args.signal ??
    findDo(Object.entries(args), (x) => {
      if (x[1] === true && numericStringRegex.exec(x[0])) {
        return parseInt(x[0], 10);
      }
    });

  const { name, number } = get(signal, 'SIGINT');

  const input = (
    await readChar(`Confirm kill all with signal ${name} (${number}) (y/n): `)
  )?.toLowerCase();

  if (input !== 'y') {
    console.error('Exiting per user input', input);

    return;
  }

  let failures = 0;

  for (const { pid } of list) {
    debug(
      'killing process ID',
      pid,
      'with signal',
      name,
      'and signal number',
      number,
    );

    try {
      process.kill(pid, name);
    } catch (err) {
      ++failures;
      debug('failed due to', err);
    }

    if (failures > 0) {
      throw new Error(
        `of the ${list.length} process to kill, ${failures} attempts failed`,
      );
    }
  }
};

if (require.main === module) {
  (async () => {
    try {
      await kg(minimist(process.argv.slice(2), minimistOpts));
    } catch (err) {
      console.error(err);

      process.exit(1);
    }
  })();
}
