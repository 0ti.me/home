#!/usr/bin/env node

const delay = require('../delay.cjs');
const fs = require('fs');
const minimist = require('minimist');
const spawn = require('../spawn-promise.cjs');
const uuid = require('uuid').v4;

const logger = console;

const FILE_PATH = '/tmp/rlt.pos.txt';
const LOCK_FILE = `${FILE_PATH}.lock`;

const daysOfGenocide =
  1 + Math.floor((new Date() - new Date(2023, 9, 7)) / (3600 * 24000));

const LIST = [
  `Day ${daysOfGenocide}: Genocide Joe is complicit with Israeli war crimes in Gaza. Dems and Repubs in Congress are also complicit.`,
  '>27,000 Gazan civilians murdered by so-called Israel since October 7th, more by starvation and disease. Ceasefire NOW!',
  'The IOF claimed they had killed 60 Hamas operatives out of the then 10,000 dead. That is a 99.5% civilian death toll.',
  'ICJ found sufficient evidence to investigate so-called Israel for the crime of genocide against the Palestinian people.',
  'Palestine tried peaceful protest in 2018, and the world ignored them. Watch Gaza Fights for Freedom.',
  "It's not war, it's a genocide. From the river to the sea, Palestine will be free! Watch Salt of this Sea on Nflx.",
  'In order to show your solidarity with Gaza, call your political leaders to demand a ceasefire now!',
  'The Electronic Intifada, meaning Electronic Uprising, is a great source of Palestinian voices and info!',
  "So-called Israel's genocide and ethnic cleansing started even before 1948 and continue today. 75 years of occupation.",
  `85% of Gazan civilians have been displaced by so-called Israel's actions over these ${daysOfGenocide} days of genocide.`,
  'So-called Israel is obligated under intl law to protect civilians - they intentionally kill civilians and journalists.',
];

const responses = {
  antiSemitism:
    'Conflating zionism or the apartheid so-called State of Israel with jews in general is itself anti-semitic.',
  babies:
    "How many babies need to die before you start to care? 11,000 dead children isn't enough for you to care?",
  broodWar: 'This is Starcraft: Brood War!',
  doNotCare:
    "Remember 1940 Germany? When we witness an injustice and don't act, we train our character to be passive in its presence.",
  hamas:
    'Someone broke into my house holding me at gunpoint until the police negotiated two-house solution - I keep the basement.',
  hopeForYou:
    'I hope when you are the victim of violence like this probably from climate change that people rally to your defense',
  oneState:
    "Israel's leaders want one apartheid State excluding Palestinians, Palestine's - a free, fair, and secular State for all.",
  oppressors:
    'In the western psyche, the Israelis can never be seen to be committing crimes as severe as the germans.',
  protestPurpose:
    "The point of protest is inconvenience, so your reaction proves it's working. You're clearly uncomfortable.",
  religiousConflict:
    'This is not rooted in religious conflict. Israel slaughters Palestinian Jews and Christians, too.',
  trembleIndignation:
    'If you tremble with indignation at every injustice then you are a comrade of mine.',
};

const MAX_LENGTH = 120;

const lock = async (val = uuid()) => {
  if (fs.existsSync(LOCK_FILE)) {
    // stop the other execution also
    unlock();

    throw new Error('aborting because locked');
  }

  fs.writeFileSync(LOCK_FILE, val, 'utf-8');

  await new Promise((res) => setTimeout(res, 50));

  if (fs.readFileSync(LOCK_FILE, 'utf-8') !== val) {
    throw new Error('aborting for lock file value mismatch');
  }
};

const unlock = () => {
  try {
    fs.rmSync(LOCK_FILE);
  } catch (err) {}
};

const rocketLeagueTypeTextV2Impl = async (text) => {
  await spawn('/usr/bin/xdotool', ['key', 't', 'type', '--delay', '8', text]);

  await delay(15);

  await spawn('/usr/bin/xdotool', ['key', 'enter']);
};

const rocketLeagueTypeTextV2 = async (args) => {
  let indx = -1;
  let item;

  await lock();

  try {
    indx = parseInt(fs.readFileSync(FILE_PATH, 'utf-8'), 10);
  } catch (err) {
    // ignore
  }

  const val = args._[0];

  switch (val) {
    case 'reset':
      indx = -1;
    // fallthrough intentionally
    case 'next':
      ++indx;
      item = LIST[indx];
      break;
    default:
      console.error(args);
      item = responses[val];
      break;
  }

  if (!item) {
    throw new Error(`no item for response ${args[0]}`);
  }

  if (item.length > MAX_LENGTH) {
    console.error(
      `warn! too long: ${item}, truncates to ${item.substr(0, MAX_LENGTH)}`,
    );
  }

  await spawn('/usr/bin/xdotool', ['keyup', 'ctrl']);

  await delay(200);

  rocketLeagueTypeTextV2Impl(item);

  fs.writeFileSync(FILE_PATH, indx.toString(), 'utf-8');
};

if (require.main === module) {
  rocketLeagueTypeTextV2(minimist(process.argv.slice(2)))
    .catch((err) => {
      logger.error(err);

      return process.exit(1);
    })
    .then(() => setTimeout(unlock, 150));
}
