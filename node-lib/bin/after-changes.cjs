#!/usr/bin/env node

const monChanges = require('./mon-changes.cjs');

const minimist = require('minimist');

const untilChanges = (args) => {
  return monChanges({
    max: 1,
    ...args,
  });
};

module.exports = untilChanges;
module.exports.default = untilChanges;

if (require.main === module) {
  untilChanges(minimist(process.argv.slice(2))).catch((err) => {
    logger.error(err);
    return process.exit(1);
  });
}
