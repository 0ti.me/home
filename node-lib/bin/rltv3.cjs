#!/usr/bin/env node

const delay = require('../delay.cjs');
const fs = require('fs');
const minimist = require('minimist');
const config = require('../config/local');
const spawnPromise = require('../spawn-promise.cjs');
const path = require('path');
const uuid = require('uuid').v4;

const logger = console;

const FILE_PATH = '/tmp/rlt.pos.txt';
const LOCK_FILE = `${FILE_PATH}.lock`;
const ERROR_PATH = '/tmp/rlt.err.txt';

const selectedConfig = 'palestine';

const customMap = config[selectedConfig];

const LIST = config.lists[selectedConfig];

const MAX_LENGTH = 120;

const lock = async (val = uuid()) => {
  if (fs.existsSync(LOCK_FILE)) {
    // stop the other execution also
    unlock();

    throw new Error('aborting because locked');
  }

  fs.writeFileSync(LOCK_FILE, val, 'utf-8');

  await new Promise((res) => setTimeout(res, 50));

  if (fs.readFileSync(LOCK_FILE, 'utf-8') !== val) {
    throw new Error('aborting for lock file value mismatch');
  }
};

const unlock = () => {
  try {
    fs.rmSync(LOCK_FILE);
  } catch (err) {}
};

const rocketLeagueTypeTextV3Impl = async (text) => {
  await spawnPromise('/usr/bin/xdotool', [
    'key',
    't',
    'type',
    '--delay',
    '8',
    text,
  ]);

  await delay(15);

  await spawnPromise('/usr/bin/xdotool', ['key', 'enter']);
};

const splitter = (longText) => {
  const items = [];
  let indx = 0;
  let start = 0;

  const p = (slice) => {
    console.error('pushing slice', slice.length, slice);
    items.push(slice);
    return slice;
  };

  while (true) {
    start = indx;
    indx += 120;

    if (indx > longText.length) {
      p(longText.slice(start, longText.length));
      break;
    }

    for (; indx > start; --indx) {
      if (longText[indx] === ' ') {
        p(longText.slice(start, indx));
        ++indx;
        break;
      }
    }

    if (indx <= start) {
      indx = start + 120;
      p(longText.slice(start, indx));
    }
  }

  return items;
};

const rocketLeagueTypeTextV3 = async (args) => {
  let indx = -1;
  let item;

  await lock();

  try {
    indx = parseInt(fs.readFileSync(FILE_PATH, 'utf-8'), 10);
  } catch (err) {
    // ignore
  }

  const val = args._[0];

  switch (val) {
    case 'reset':
      indx = -1;
    // fallthrough intentionally
    case 'next':
      ++indx;
      item = LIST[indx];
      break;
    case 'responseSelector':
      item = 'no output';

      await spawnPromise(
        path.join(__dirname, '../../../tmp/node_modules/.bin/qode'),
        [
          '--no-warnings',
          path.join(__dirname, '../../../tmp/dist/index.js'),
          '--config',
          JSON.stringify(config),
        ],
      ).stdout.on('data', (data) => {
        item = data.toString().trim();
      });

      console.error(148, item, customMap[item]);

      item = customMap[item];

      break;
    default:
      console.error(args);
      item = customMap[val];
      break;
  }

  if (!item) {
    throw new Error(`no item for response ${args[0]}`);
  }

  const items = item.length > MAX_LENGTH ? splitter(item) : [item];

  await spawnPromise('/usr/bin/xdotool', ['keyup', 'ctrl']);

  await delay(200);

  let first = true;

  for (const item of items) {
    if (!first) {
      await delay(10);
    }

    first = false;

    await rocketLeagueTypeTextV3Impl(item);
  }

  fs.writeFileSync(FILE_PATH, indx.toString(), 'utf-8');
};

if (require.main === module) {
  let returnCode = 0;

  rocketLeagueTypeTextV3(minimist(process.argv.slice(2)))
    .catch((err) => {
      logger.error(err);
      fs.writeFileSync(ERROR_PATH, err.stack, 'utf-8');

      returnCode = 1;
    })
    .then(() => delay(150))
    .then(() => unlock())
    .then(() => process.exit(returnCode));
}
