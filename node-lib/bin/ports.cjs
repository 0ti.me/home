#!/usr/bin/env node

const minimist = require('minimist');
const spawnPromise = require('../spawn-promise.cjs');
const stream = require('stream');

const numeric = /^[0-9]+$/;
const portDefinition = /^[a-f0-9.:]*:[0-9]{1,5}$/;

class FilterStream extends stream.Writable {
  constructor(filterCb) {
    super();
    this.filterCb = filterCb;
  }

  _write(chunk, enc, next) {
    const chunkString = chunk.toString();

    if (!this.filterCb) {
      process.stdout.write(chunkString);
    } else {
      const lines = chunkString.split('\n');

      lines.slice(0, lines.length - 1).forEach((line) => {
        if (this.filterCb(line)) {
          // console because we stripped newlines
          console.log(line);
        }
      });
    }

    next();
  }
}

const ports = async (args) => {
  let filterStream;

  if (args && args._ && args._.length > 0) {
    const numericArgs = args._.filter((arg) => numeric.test(arg)).map((ea) =>
      ea.toString(),
    );

    filterStream = new FilterStream((line) => {
      const pieces = line.split(/[ \t]+/);

      return (
        !pieces[3] ||
        !portDefinition.test(pieces[3]) ||
        numericArgs.find((arg) => pieces[3].slice(-arg.length) === arg)
      );
    });
  }

  let spawnedProcPromise = spawnPromise('netstat', [
    '-ltnp',
  ]).pipeStderrToConsole();

  if (filterStream) {
    spawnedProcPromise = spawnedProcPromise.stdout.pipe(filterStream);
  } else {
    spawnedProcPromise = spawnedProcPromise.pipeStdoutToConsole();
  }

  return await spawnedProcPromise;
};

if (require.main === module) {
  (async () => {
    try {
      await ports(minimist(process.argv.slice(2)));
    } catch (err) {
      console.error(err);

      process.exit(1);
    }
  })();
}
