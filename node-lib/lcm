#!/usr/bin/env node

const {isMultiple} = require('./math');
const {primes} = require('./primes');

const lcm = array => {
  const max = array.reduce((acc, ea) => acc > ea ? acc : ea);
  const primeValues = primes(max);

  const multiples = array.map(ea => {
    const result = [];

    while (!primeValues.includes(ea)) {
      const primeValue = primeValues.find(primeValue => isMultiple(ea, primeValue));

      if (primeValue) {
        result.push(primeValue);
        ea /= primeValue;
      } else {
        throw new Error(`${primeValue} was returned for ${ea}`);
      }
    }

    result.push(ea);

    return result;
  }).reduce((set1, set2) => {
    const result = [];

    while (set1.length > 0 || set2.length > 0) {
      if (set1.length === 0) {
        result.push(set2.shift());
      } else if (set2.length === 0) {
        result.push(set1.shift());
      } else if (set1[0] > set2[0]) {
        result.push(set2.shift());
      } else if (set1[0] < set2[0]) {
        result.push(set1.shift());
      } else if (set1[0] === set2[0]) {
        result.push(set1.shift());
        set2.shift();
      } else {
        throw new Error('oops');
      }
    }

    return result;
  });

  return multiples.reduce((a, b) => a * b);
};

if (require.main === module) {
  console.log(lcm(process.argv.slice(2).map(ea => parseInt(ea, 10))));
}
