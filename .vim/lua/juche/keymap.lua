-- Thanks prime!
local M = {}

local function bind(op, outer_opts)
    outer_opts = outer_opts or {noremap = true}
    return function(lhs, rhs, opts)
        opts = vim.tbl_extend("force",
            outer_opts,
            opts or {}
        )
        vim.keymap.set(op, lhs, rhs, opts)
    end
end

M.nmap = bind("n", {noremap = false})
M.nnoremap = bind("n")
M.vnoremap = bind("v")
M.xnoremap = bind("x")
M.inoremap = bind("i")

-- better searches
-- M.nnoremap('/', '/\v');
-- M.vnoremap('/', '/\v');

vim.keymap.set('n', '<SPACE>', '<Nop>')
vim.keymap.set('v', '<SPACE>', '<Nop>')
vim.g.mapleader = ' '

M.nnoremap("<leader>e", "<cmd>lua vim.diagnostic.open_float(0, {scope=\"line\"})<cr>")
M.nnoremap("<leader>n", "<cmd>bnext<cr>")
M.nnoremap("<leader>p", "<cmd>bprevious<cr>")
M.nnoremap("<leader>s", "<cmd>Sort<cr>")
M.vnoremap("<leader>s", "<esc><cmd>Sort<cr>")
M.nnoremap("<leader>t", "<C-^>")
M.nnoremap('<leader>q', ':bd<cr>')
M.nnoremap('<leader>y', '"+y')
M.nnoremap('<leader>Y', '"+y$')
M.nnoremap('<leader>x', '"+x')
M.nnoremap('<leader>dd', '"+dd')
M.nnoremap('<leader>D', '"+D')
M.nnoremap('<leader>w', '@q')
-- ()
M.nnoremap('<leader>(w', 'ciw()<esc>P')
M.nnoremap('<leader>)w', 'ciw()<esc>P')
M.nnoremap('<leader>(l', '|C()<esc>P')
M.nnoremap('<leader>)l', '|C()<esc>P')
M.nnoremap('<leader>d(', 'di("_da(p')
M.nnoremap('<leader>d)', 'di("_da(p')
-- []
M.nnoremap('<leader>[w', 'ciw[]<esc>P')
M.nnoremap('<leader>]w', 'ciw[]<esc>P')
M.nnoremap('<leader>[l', '|C[]<esc>P')
M.nnoremap('<leader>]l', '|C[]<esc>P')
M.nnoremap('<leader>d[', 'di["_da[p')
M.nnoremap('<leader>d]', 'di["_da[p')
-- {}
M.nnoremap('<leader>{w', 'ciw{}<esc>P')
M.nnoremap('<leader>{w', 'ciw{}<esc>P')
M.nnoremap('<leader>{l', '|C{}<esc>P')
M.nnoremap('<leader>}l', '|C{}<esc>P')
M.nnoremap('<leader>d{', 'di{"_da{p')
M.nnoremap('<leader>d}', 'di{"_da{p')
-- ""
M.nnoremap('<leader>"w', 'ciw""<esc>P')
M.nnoremap('<leader>"l', '|C""<esc>P')
M.nnoremap('<leader>d"', 'di""_da"p')
-- ''
M.nnoremap('<leader>\'w', 'ciw\'\'<esc>P')
M.nnoremap('<leader>\'l', '|C\'\'<esc>P')
M.nnoremap('<leader>d\'', 'di\'"_da\'p')
-- <>
M.nnoremap('<leader><w', 'ciw<><esc>P')
M.nnoremap('<leader>>w', 'ciw<><esc>P')
M.nnoremap('<leader><l', '|C<><esc>P')
M.nnoremap('<leader>>l', '|C<><esc>P')
M.nnoremap('<leader>d<', 'di<"_da<p')
M.nnoremap('<leader>d>', 'di<"_da<p')

-- Allows you to <leader>rc in order to run the command on the current line, removing comment stuff at the beginning
-- e /tmp/experiment.sh
M.nnoremap("<leader>rc", ':exe substitute(getline("."), "^[ ]*[#/-]*[ ]*", "", "")<cr>')

return M
