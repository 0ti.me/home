-- Install packer if not present
-- run :PackerSync in neovim to redo the sync
local fn = vim.fn
local install_path = fn.stdpath("data").."/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({"git", "clone", "--depth", "1", "https://github.com/wbthomason/packer.nvim", install_path})
  vim.cmd [[packadd packer.nvim]]
end

local packer = require("packer")

return packer.startup(function(use)
  use("sbdchd/neoformat")
  use("wbthomason/packer.nvim")
  use({
    "windwp/nvim-autopairs",
    config = function() require("nvim-autopairs").setup {} end
  })
  use("tomtom/tcomment_vim")
  use("nvim-lua/plenary.nvim")
  use("nvim-lua/popup.nvim")
  use("nvim-telescope/telescope.nvim")
  use("neovim/nvim-lspconfig")
  use("hrsh7th/cmp-nvim-lsp")
  use("hrsh7th/cmp-buffer")
  use("hrsh7th/nvim-cmp")
  use("nvim-lua/lsp_extensions.nvim")
  use("L3MON4D3/LuaSnip")
  use("saadparwaiz1/cmp_luasnip")
  use 'tpope/vim-sleuth'
  use(
    "nvim-treesitter/nvim-treesitter",
    {
      run = ":TSUpdate"
    }
  )
  use("nvim-treesitter/playground")
  use("kyazdani42/nvim-web-devicons")
  use({
    "nvim-lualine/lualine.nvim",
    requires = { "kyazdani42/nvim-web-devicons", opt = true }
  })
  use({
    "lewis6991/gitsigns.nvim",
    config = function()
      require("gitsigns").setup()
    end
  })
  use({
    "sindrets/diffview.nvim",
    requires = "nvim-lua/plenary.nvim"
  })
  use({
    "TimUntersberger/neogit",
    requires = {
      "nvim-lua/plenary.nvim",
      "sindrets/diffview.nvim"
    }
  })

  use({
    'sQVe/sort.nvim',
    -- Optional setup for overriding defaults.
    config = function()
      require("sort").setup({
      })
    end
  })

  -- colorschemes
  use("folke/tokyonight.nvim")
  use("cocopon/iceberg.vim")
  use("EdenEast/nightfox.nvim")
  use({ "catppuccin/nvim", as = "catppuccin" })
  use({ "embark-theme/vim", as = "embark" })
  use({
      "rose-pine/neovim",
      as = "rose-pine",
  })
  use('jose-elias-alvarez/null-ls.nvim')
  use('MunifTanjim/prettier.nvim')
  use('yunlingz/equinusocio-material.vim')
  use('w0rp/ale')
  use('williamboman/mason.nvim')
  use('WhoIsSethDaniel/mason-tool-installer.nvim')

  use('Decodetalkers/csharpls-extended-lsp.nvim')
  use('Hoffs/omnisharp-extended-lsp.nvim')

  -- Gives :%S to keep case while replacing
  use('tpope/vim-abolish')
  -- Gives :[range]Yankitute[register]/{pattern}/[string]/[flags]/[join]
  use('vim-scripts/Yankitute')

  use({'neoclide/coc.nvim', branch = 'release'})
  use('prisma/vim-prisma')

  -- Autoload plugins on fresh install
  if packer_bootstrap then
    -- run :PackerSync in neovim to redo the sync
    packer.sync()
  end
end)
