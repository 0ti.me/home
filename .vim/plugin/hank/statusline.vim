set statusline=%F%m%r%h%w\ %{&ff}\ %Y\ ASCII=\%03.3b\ 0x\%02.2B\ %l,%v[%p%%]\ LINES=%L
set laststatus=2
