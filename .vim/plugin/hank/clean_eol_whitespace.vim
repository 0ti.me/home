autocmd BufWritePre * let w:wv = winsaveview() | keeppatterns %s/\s\+$//e | call winrestview(w:wv)
