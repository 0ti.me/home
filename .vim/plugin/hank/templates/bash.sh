#!/usr/bin/env bash

main() {
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"
}

if \
  ( [ -n "$ZSH_VERSION" ] && ! [[ $ZSH_EVAL_CONTEXT =~ :file$ ]] ) \
  || [[ "${BASH_SOURCE[0]}" == "$0" ]]; then
  main "$@"
fi
