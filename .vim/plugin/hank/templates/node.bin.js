#!/usr/bin/env node

const {isEmpty} = require('lodash');
const minimist = require('minimist');
const Promise = require('bluebird');

const renameMe = args =>
  Promise.try(() => {
    // INSERT_YOUR_CODE_HERE
    return "Hello World";
  });

if (require.main === module) {
  Promise.try(() => minimist(process.argv.slice(2)))
    .then(renameMe)
    .then(result => isEmpty(result) || console.log(result))
    .catch(err => {
      console.error(err);

      return -1;
    })
    .then(process.exit);
} else {
  module.exports = renameMe;
}
