if has("autocmd")
  function s:ReplaceWithTemplate(template)
    " Expand the template filename
    let template_file = expand("$HOME/.vim/plugin/hank/templates/") . a:template
    "echo template_file

    " Insert the template file contents at the beginning of the file
    execute "0r " . template_file

    " Restart syntax highlighter in case a shebang fixes syntax highlighting
    syn on

    " Remove the last line in the file then go back to the beginning
    normal! G
    normal! dd
    normal! H
  endf

  augroup templates
    " .sh and .bash
    autocmd BufNewFile *.sh,*.bash call s:ReplaceWithTemplate("bash.sh")

    " .bin.js (Executable NodeJS scripts)
    autocmd BufNewFile *.bin.js call s:ReplaceWithTemplate("node.bin.js")
  augroup END
endif
