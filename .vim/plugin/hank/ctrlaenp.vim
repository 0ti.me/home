map <C-a> <Home>
map <C-e> <End>
map <C-n> :tabnext<cr>
map <C-p> :tabprev<cr>
map <C-o> :tabnext<cr>

imap <C-a> <Home>
imap <C-e> <End>
imap <C-n> <Esc>:tabnext<cr>a
imap <C-p> <Esc>:tabprev<cr>a
imap <C-o> <Esc>:tabnext<cr>a
