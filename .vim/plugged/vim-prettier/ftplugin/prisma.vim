let b:prettier_ft_default_args = {
  \ 'parser': 'prisma',
  \ }

augroup Prettier
  autocmd!
  if g:prettier#autoformat
    autocmd BufWritePre *.prisma call prettier#Autoformat()
  endif
augroup end
