augroup Prettier
  autocmd!
  if g:prettier#autoformat
    autocmd BufWritePre *.js,*.jsx,*.mjs,*.tsx call prettier#Autoformat()
  endif
augroup end
