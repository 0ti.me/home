vim.g.ale_fixers = {
  javascript = {'prettier'},
  typescript = {'prettier'}
}

vim.cmd('let g:ale_pattern_options = {".prisma$": {"ale_enabled": 0}, ".cs$": {"ale_enabled": 0}}')

vim.g.ale_fix_on_save = 1
