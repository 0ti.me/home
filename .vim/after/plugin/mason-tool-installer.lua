require('mason-tool-installer').setup({
  ensure_installed = {
    "arduino-language-server",
    "bash-language-server",
    "css-lsp",
    "eslint-lsp",
    "lua-language-server",
    "marksman",
    "typescript-language-server",
  }
})
