#!/usr/bin/env bash

set -ex

main() {
  local PERIOD=${PERIOD-0.5}
  local SEARCH="${SEARCH-"minecraft.*1."}"
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"
  local WINDOW="$(xdotool search --name "minecraft.*1\.")"

  while [ $# -gt 0 ]; do
    case $1 in
      --period)
        PERIOD=$2
        shift
        ;;
      --period=*)
        PERIOD="$(echo "${1}" | sed 's/^--period=//')"
        ;;
      --search)
        SEARCH=$2
        shift
        ;;
      --search=*)
        SEARCH="$(echo "${1}" | sed 's/^--search=//')"
        ;;
    esac

    shift
  done

  if [ "${SEARCH}" = "" ]; then
    >&2 echo "Couldn't find search term"
  fi

  if [ "${WINDOW}" = "" ]; then
    >&2 echo "Couldn't find window with search term ${SEARCH}"
    return 1
  fi

  if [ "${PERIOD}" = "" ]; then
    return -1
  fi

  while true; do
    sleep ${PERIOD}

    xdotool click 1
  done
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  main "$@"
fi
