#!/usr/bin/env bash

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  errcho "You should not source this script ($( \
    basename "${BASH_SOURCE[0]:-${(%):-%x}}"))"

  return -1
fi

set -e

init() {
  local ENV_FILE="${HOME}/.env"
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"
  local BASHRC_D="${THIS_DIR}/.bashrc.d"
  local BIN_D="${THIS_DIR}/bin"
  local LOG_FILE="${THIS_DIR}/install.sh.log"
  local ROOT_DIR="${THIS_DIR}"

  . "${BASHRC_D}/each"

  . "${BASHRC_D}/indent"
  . "${BASHRC_D}/os"
  . "${BASHRC_D}/unindent"
  . "${BIN_D}/ensure-link"
  . "${BIN_D}/errcho"
  . "${BIN_D}/ierrcho"
  . "${BIN_D}/interactive"

  touch "${HOME}/.vimrc"

  if [ -f "${ENV_FILE}" ]; then
    >&2 echo "$0:19 loading env file with contents:"

    cat "${ENV_FILE}"

    while read LINE; do
      export "${LINE}"
    done < "${ENV_FILE}"
  fi

  local CONF=
  local EACH=
  local LINK=
  local LINK_BASENAME=
  local PKGS=()
  local TABSIZE=0
  local THIS="$(basename "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"

  indent "${THIS}"

  indent "Updating .bashrc"
  find .bashrc/ -type f \
    | sort \
    | while read EACH; do
      PATH="${BIN_D}:${PATH}" ${EACH}
    done
  unindent

  indent "Updating .zshrc"
    "${THIS_DIR}/bin/block-in-file" \
      --file-target "${HOME}/.zshrc" \
      --text-block '. "${HOME}/.profile"' \
      --text-context "source profile"
  unindent

  indent "Ensuring paths"
  for EACH in "$HOME/.local/bin"; do
    mkdir -p "${EACH}"
  done
  unindent

  indent "Ensuring symlinks"
  for LINK in .bashrc.d .tmux.conf .vim .xbindkeysrc bin; do
    ensure-link "${THIS_DIR}/${LINK}" "${HOME}/${LINK}"
  done

  mkdir -p "${HOME}/.config"
  ensure-link "${THIS_DIR}/.vim" "${HOME}/.config/nvim"

  unindent

  # these probably can't have spaces? Not sure. Ideally just letters.
  for alias_ in "$(ls "${THIS_DIR}/aliases")"; do
    WORKING="${THIS_DIR}/aliases/${alias_}"

    "${THIS_DIR}/bin/block-in-file" \
      --file-target "${HOME}/.profile" \
      --text-block "alias ${alias_}='$(cat "${WORKING}")'" \
      --text-context "mcd-alias"
  done

  sudo "${THIS_DIR}/bin/block-in-file" \
    --file-target "/etc/environment" \
    --text-block "EDITOR=vim" \
    --text-context "EDITOR=vim"

  if macos; then
    "${THIS_DIR}/bin/block-in-file" \
      --file-target "${HOME}/.bash_profile" \
      --text-block "$(echo -e \
          '[ -f "/etc/profile" ] && PATH="" && . "/etc/profile"' \
          '\n\n[ -f "${HOME}/.profile" ] && . "${HOME}/.profile"' \
          | sed 's/ *$//g' \
        )" \
      --text-context "macos-profile"
  fi

  indent "ensure/sudo"
  "${THIS_DIR}/ensure/sudo"
  unindent

  if [ "${CAN_SUDO-1}" -eq 1 ]; then
    indent "sudoers.d.sh"
    "${THIS_DIR}/sudoers.d.sh"
    unindent
  fi

  if linux; then
    mkdir -p "${HOME}/.config"
    ensure-link \
      "${THIS_DIR}/terminator" \
      "${HOME}/.config/terminator"

    if which apt >/dev/null; then
      # Initial dependency gathering
      apt-install-any-or-all config/apt1

      apt-install-any-or-all config/apt2 --no-install-recommends
    elif which pamac >/dev/null; then
      pamac install --upgrade --no-confirm \
        jq \
        python-pip \
        ripgrep \
        terminator
    fi
  elif macos; then
    if which brew >/dev/null; then
      PKGS=(bash bash-completion bash-language-server coreutils git jq lua-language-server mysql neovim rg yarn)

      brew install "${PKGS[@]}" \
        || brew upgrade "${PKGS[@]}"

      "${THIS_DIR}/bin/block-in-file" \
        --after-text "macos-profile END" \
        --file-target "${HOME}/.bash_profile" \
        --text-block '[[ -r "/usr/local/etc/profile.d/bash_completion.sh" ]] && . "/usr/local/etc/profile.d/bash_completion.sh"' \
        --text-context "bash-completion"
    fi
  fi

  indent "sysctl"
  # For some reason the inotify stuff in sysctl.d doesn't seem to work so just do it this way..
  if [ "$(cat /etc/sysctl.conf | grep -E '^fs.inotify.max_user_watches' | tail -n 1 | awk1 -f=)" -lt 524288 ]; then
    echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
  fi

  sudo ln -s /etc/sysctl.conf /usr/lib/sysctl.d/99-sysctl.conf

  while read EACH; do
    if echo "${EACH}" | grep -E '\.sh$' 2>/dev/null >&2; then
      "${EACH}"
    fi 2>&1 | each ${BIN_D}/ierrcho
  done < <(find sysctl -type f)
  unindent

  if which git >/dev/null; then
    indent "Configuring git"
    for CONF in \
      "core.editor vim" \
      "fetch.prune true" \
      "init.defaultBranch trunk" \
      "pull.rebase true" \
      "push.default current" \
      "push.followTags true" \
      "rebase.autostash true" \
      "remote.origin.prune true" \
      ; do
      ierrcho --replace-all --global $CONF
      git config --replace-all --global $CONF
    done
    unindent
  fi

  indent "Installing"
  while read EACH; do
    indent "$(basename ${EACH}): "
    "${EACH}"
    unindent
  done < <(find "${THIS_DIR}/install" -type f | sort)

  . ~/.nvm/nvm.sh
  unindent

  if ! which yarn 2>/dev/null >&2; then
    npm install --global nodemon prettier tree-sitter-cli typescript yarn
  fi

  yarn global add ts-node@9.1.1
  yarn config set --home enableTelemetry 0

  indent "Configuring"
  while read EACH; do
    ierrcho "$(basename ${EACH}):"

    "${EACH}"
  done < <(find "${THIS_DIR}/configure" -type f)
  unindent

  indent "yarn install"
  yarn
  unindent

  if [ -d "${HOME}/.kde" ]; then
    indent "kde.config"
    find kde.home.config -mindepth 1 -maxdepth 1 | while read EACH; do
      ensure-link "${ROOT_DIR}/${EACH}" "${HOME}/.config/$(basename "${EACH}")"
    done
    unindent
  fi

  unindent
  ierrcho "${THIS}: Complete"
}

apt-install-any-or-all() {
  local EACH=
  local FILE="${1}"
  shift

  indent '`apt install`-ing contents of '"${FILE}"
  cat "${FILE}" | while read EACH; do
    if ! "${BIN_D}/apt-installed" "${EACH}"; then
      sudo apt update && \
        sudo apt install -y "$@" $(cat "${FILE}")

      break
    fi
  done
  unindent
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  init "$@"
fi
