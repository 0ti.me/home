#!/usr/bin/env bash

update() {
  set -e

  local CHK=
  local CHK_BIN=
  local INIT_DIR="$(pwd)"
  local MARK="ebdab33822bd-dirty-ebdab33822bd"
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"
  local THIS_FILE=

  if ${THIS_DIR}/bin/is-relative-path "${BASH_SOURCE[0]:-${(%):-%x}}"; then
    THIS_FILE="${INIT_DIR}/${BASH_SOURCE[0]:-${(%):-%x}}"
  else
    THIS_FILE="${BASH_SOURCE[0]:-${(%):-%x}}"
  fi

  cd "${THIS_DIR}"

  if git describe --all --dirty="${MARK}" \
    | grep "${MARK}" >/dev/null; then
    echo "Refusing to update ${HOSTNAME}:${THIS_DIR} due to unstaged changes" >&2
    git status

    exit -1
  fi

  if which sha256sum >/dev/null; then
    CHK_BIN=sha256sum
  elif which md5 >/dev/null; then
    CHK_BIN=md5
  elif [ -x /usr/local/bin/sha256sum ]; then
    CHK_BIN=/usr/local/bin/sha256sum
  else
    ${THIS_DIR}/bin/errcho "No checksum binary found"

    exit -1
  fi

  CHK=$("${CHK_BIN}" "${THIS_FILE}")

  "${THIS_DIR}/bin/print-if-fail" git pull

  if [ "$("${CHK_BIN}" "${THIS_FILE}")" != "${CHK}" ]; then
    "${0}" "$@"
  else
    "${THIS_DIR}/install.sh"
  fi
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  update "$@"
fi
