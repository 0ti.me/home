#!/usr/bin/env bash

main() {
  local DEST=/tmp/vim-module-${RANDOM}
  local EACH=
  local FINAL_DEST=~/.vim
  local REPO=
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"

  while [ $# -gt 0 ]; do
    case $1 in
      --repo)
        REPO="${2}"
        shift
        ;;
      --repo=*)
        REPO="$(echo "${1}" | sed 's/--repo=//')"
        ;;
    esac

    shift
  done

  git clone "${REPO}" "${DEST}"

  mkdir -p "${FINAL_DEST}"

  for EACH in autoload compiler doc ftdetect ftplugin indent plugin syntax; do
    if [ -d "${DEST}/${EACH}" ]; then
      cp -r "${DEST}/${EACH}" "${FINAL_DEST}/"
    fi
  done

  rm -rf "${DEST}"
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  main "$@"
fi
