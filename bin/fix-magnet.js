#!/usr/bin/env node

const qs = require('../node-lib/querystring');

const opts = {encode: false};

for (const ea of process.argv.slice(2)) {
  const obj = qs(ea);

  obj.dn = obj.dn
    .replace(/[-+:\(\).\[\]]/g, '.')
    .replace(/[.]+/g, '.')
    .replace(/[.]+$/, '')
    .replace(/^[.]+/, '')
    .substr(0, 96)
    .replace(/[.]+$/, '');

  console.log(`magnet:?${qs.toQueryString(obj, opts)}`);
}
