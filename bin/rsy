#!/usr/bin/env bash

count-rsync-procs() {
  ps -ax | grep '[ ]rs[y]nc' | wc -l
}

rsy() {
  local MAX=1
  local RSYNC_CMD=(rsync --partial --archive --progress --verbose)
  local RSYNC_COUNT="$(count-rsync-procs)"
  local RTRN_CODE=0
  local SLEEP_LEN=15

  while [ $# -gt 0 ]; do
    case "$1" in
      --force)
        MAX=0
        ;;
      --max)
        shift
        MAX=$1
        ;;
      *)
        RSYNC_CMD+=("$1")
        ;;
    esac
    shift
  done

  >&2 echo "Will execute:"
  >&2 echo "+$(printf " %q" "${RSYNC_CMD[@]}")"

  local LOOP_COUNT=0
  RSYNC_COUNT=${RSYNC_COUNT:-99}
  while [ "${MAX}" -gt 0 ] && [ $RTRN_CODE -eq "${MAX}" ] && [ ${RSYNC_COUNT} -gt "${MAX}" ]; do
    >&2 echo "sleeping ${SLEEP_LEN} seconds waiting for ${RSYNC_COUNT} rsync processes to complete"

    sleep "${SLEEP_LEN}"

    RTRN_CODE=$?

    RSYNC_COUNT="$(count-rsync-procs)"
    RSYNC_COUNT=${RSYNC_COUNT:-99}

    let LOOP_COUNT=${LOOP_COUNT}+1
  done

  if [ $RTRN_CODE -eq 0 ]; then
    if [ "${LOOP_COUNT}" -gt 0 ]; then
      >&2 echo "executing: "
      >&2 echo "+$(printf " %q" "${RSYNC_CMD[@]}")"
    fi
    "${RSYNC_CMD[@]}"
  fi
}

rsy "$@"
