#!/usr/bin/env bash

archive-usage() {
  errcho "archive usage:"
  errcho "  archive \\"
  errcho "    --archive-name <archive-name-no-extension> \\"
  errcho "    --archive-dest <path-to-folder> \\"
  errcho "    files... dirs..."
}

archive() {
  local ARGS=( "$@" )
  local ARCHIVE_DEST=${ARCHIVE_DEST--}
  local ARCHIVE_NAME=
  local COMPRESSION=--gzip
  local DATE="$(isoutcdate)"
  local EACH=
  local ITER=0
  local FILES=( )
  local FINAL_DESTINATION=
  local NO_COMPRESSION=${NO_COMPRESSION-0}
  local TEST=

  # $# is essentially a 1-based length because it excludes $0
  while [ $ITER -le $# ]; do
    case ${ARGS[${ITER}]} in
      --archive-dest)
        ARCHIVE_DEST="${ARGS[${ITER}+1]}"

        let ITER=${ITER}+1

        ;;
      --archive-name)
        ARCHIVE_NAME="${ARGS[${ITER}+1]}"

        let ITER=${ITER}+1

        ;;
      --no-compression)
        NO_COMPRESSION=1
        ;;
      -h|--help)
        archive-usage "$0" "$@"

        return 0

        ;;
      --)
        # Ignore
        ;;
      *)
        if [ "${ARGS[${ITER}]}" != "" ]; then
          FILES+=( "${ARGS[${ITER}]}" )
        fi

        ;;
    esac

    let ITER=${ITER}+1
  done

  if [ $# -eq 1 ]; then
    ARCHIVE_DEST=.
    ARCHIVE_NAME="$(basename "$1")"
  fi

  if [ ${NO_COMPRESSION} -eq 1 ]; then
    COMPRESSION=
  fi

  if [ "${ARCHIVE_DEST}" != "-" ]; then
    if [ ${NO_COMPRESSION} -eq 1 ]; then
      FINAL_DESTINATION="${ARCHIVE_DEST}/${ARCHIVE_NAME}.${DATE}.tar"
    else
      FINAL_DESTINATION="${ARCHIVE_DEST}/${ARCHIVE_NAME}.${DATE}.tar.gz"
    fi

    mkdir -p "${ARCHIVE_DEST}" \
      || { \
        errcho "Unable to mkdir -p \"${ARCHIVE_DEST}\" to store tar archive"; \
        return -1; \
      }

    tar \
      --create \
      --file "${FINAL_DESTINATION}" \
      ${COMPRESSION} \
      "${FILES[@]}" && echo "${FINAL_DESTINATION}"
  else
    tar \
      --create \
      ${COMPRESSION} \
      "${FILES[@]}"
  fi || { \
          errcho "Unable to create tar archive"; \
          return -1; \
        }
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  archive "$@"
fi
