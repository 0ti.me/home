#!/usr/bin/env bash

main() {
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"
  local TARGET_DIR="/tmp/old.applications.$RANDOM"

  while read line; do
    if ![ -d "${TARGET_DIR}" ]; then
      mkdir "${TARGET_DIR}"
    fi

    mv -v "$line" "${TARGET_DIR}"
  done < <( \
    grep -rE steam "${HOME}/.local/share/applications" \
    | awk1 -F: \
    | sort \
    | uniq \
    | grep -v defaults.list \
  )
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  main "$@"
fi
