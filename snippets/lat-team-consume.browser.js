download = async (url, handleObjectUrl, opts = {}) => {
  const response = await fetch(url, {
    ...opts,
  });

  if (!response.ok) {
    return null;
  }

  const blob = await response.blob();

  const objectUrl = URL.createObjectURL(blob);

  handleObjectUrl(objectUrl);

  URL.revokeObjectURL(objectUrl);

  return true;
};

delay = (timeout) => new Promise((res) => setTimeout(res, timeout));

consume = async () => {
  const key = 'lat-team-hank-accumulate';
  const getItem = (key, def) => {
    try {
      return JSON.parse(localStorage.getItem(key));
    } catch (err) {
      return def;
    }
  };
  const setItem = (key, val) => {
    localStorage.setItem(key, JSON.stringify(val));
  };

  const initial = getItem(key) ?? [];
  const results = {};

  for (let i = 0; i < initial.length; ++i) {
    try {
      const url = initial[i];
      console.info('working on', url, `#${i + 1}/${initial.length}`);
      results[url] = await download(url, (x) => window.open(x));
      setItem(
        key,
        initial.filter((initialUrl) => results[initialUrl] !== true),
      );
    } catch (err) {
      console.error('failed', url, `#${i + 1}/${initial.length}`);
      console.error(err);
    }
    await delay(8000);
  }
};

consume().catch(console.error);
