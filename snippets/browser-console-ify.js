const fs = require('fs');

const fileContents = fs.readFileSync(process.argv[2], 'utf-8');

console.info(
  fileContents
    .split('\n')
    .map((ea) => ea.trim())
    .join(' '),
);
