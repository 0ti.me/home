automateSlots = (clickIntervalDelay = 500, minCredits = 300) => {
  const getWinnings = () =>
    parseInt($('#winnings').objects[0].innerHTML.replace(',', ''), 10);

  const pullSlot = () => {
    const betAmount = $('#betamount');
    const plays = $('#numbets');
    const abort = (msg) => {
      if (msg) console.info(msg);
      if (interval) clearInterval(interval);
    };

    if (betAmount !== 100 || plays !== 3) {
      return abort('set bet amount and plays first');
    }

    if (getWinnings() < minCredits) {
      return abort('running low on credits, breaking loop');
    }

    $('#lever').objects[0].click();
  };

  const interval = setInterval(() => {
    pullSlot();
  }, clickIntervalDelay);

  pullSlot();

  return interval;
};

automateSlots();
