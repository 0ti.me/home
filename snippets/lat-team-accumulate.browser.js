accumulate = () => {
  const key = 'lat-team-hank-accumulate';
  const objs = Object.values(
    document.getElementsByClassName('torrent-search--list__no-poster-row'),
  )
    .filter((x) => !x.classList.contains('torrent-search--list__sticky-row'))
    .map((ea) => $(ea).find('a[title=Download]').attr('href'));
  const dup = (x, i, l) => x && (i === 0 || l[i - 1] !== x);
  const getItem = (key, def) => {
    try {
      return JSON.parse(localStorage.getItem(key));
    } catch (err) {
      return def;
    }
  };
  const setItem = (key, val) => {
    localStorage.setItem(key, JSON.stringify(val));
  };

  const initial = getItem(key) ?? [];

  const origLen = initial.length;

  const final = initial.concat(objs).sort().filter(dup);

  setItem(key, final);

  console.error(
    'originally contained',
    origLen,
    '- now contains',
    final.length,
  );
};

accumulate();
