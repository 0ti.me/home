downloadAllSlowly = (
  initialWait = 0,
  maxPerPeriod = 25,
  intraPeriodWait = 2000,
  interPeriodWait = 300000,
) => {
  const objs = $('a[title="Download Torrent"]').objects;

  setTimeout(
    () =>
      objs.forEach((ea, i) => {
        const timeout =
          Math.floor(i / maxPerPeriod) * interPeriodWait +
          (i % maxPerPeriod) * intraPeriodWait;

        setTimeout(() => {
          console.info(
            new Date().toISOString(),
            'timeout',
            timeout,
            'Working on',
            ea,
            `(#${i} / ${objs.length})`,
            'remaining timeout before next operation',
            i + 1 === objs.length ? 'none' : nextTimeout - timeout,
          );

          ea.click();
          if (i + 1 === objs.length) {
            console.log('finished processing all downloads');
          }
        }, timeout);
      }),
    initialWait,
  );
};

downloadAllSlowly();
