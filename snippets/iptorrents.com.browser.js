downloadAllSlowly = (
  initialWait = 0,
  maxPerPeriod = 3,
  intraPeriodWait = 5000,
  interPeriodWait = 60000,
) => {
  const objs = $('.t1 a').filter((_, ea) => /\/t\/[0-9]+/.test(ea.href));

  setTimeout(
    () =>
      objs.each((i, ea) => {
        const timeout =
          Math.floor(i / maxPerPeriod) * interPeriodWait +
          (i % maxPerPeriod) * intraPeriodWait;
        const nextTimeout =
          Math.floor((i + 1) / maxPerPeriod) * interPeriodWait +
          ((i + 1) % maxPerPeriod) * intraPeriodWait;

        setTimeout(() => {
          console.info(
            new Date().toISOString(),
            'timeout',
            timeout,
            'Working on',
            ea,
            `(#${i} / ${objs.length})`,
            'remaining timeout before next operation',
            i + 1 === objs.length ? 'none' : nextTimeout - timeout,
          );

          const id = ea.href.replace(/^.*\/t\//, '');
          const htmlifiedName = encodeURIComponent(ea.innerText);

          window.location.href = `download.php/${id}/${htmlifiedName}.torrent`;

          if (i + 1 === objs.length) {
            console.log('finished processing all downloads');
          }
        }, timeout);
      }),
    initialWait,
  );
};

downloadAllSlowly();
