downloadAllSlowly = (
  initialWait = 0,
  maxPerPeriod = 99999,
  intraPeriodWait = 10000,
  interPeriodWait = 0,
) => {
  const objs = Object.values(
    document.getElementsByClassName('torrent-search--list__no-poster-row'),
  )
    .filter((x) => !x.classList.contains('torrent-search--list__sticky-row'))
    .map((ea) => $(ea).find('a[title=Download]').attr('href'));

  setTimeout(
    () =>
      objs.forEach((ea, i) => {
        const timeout =
          Math.floor(i / maxPerPeriod) * interPeriodWait +
          (i % maxPerPeriod) * intraPeriodWait;
        const nextTimeout =
          Math.floor((i + 1) / maxPerPeriod) * interPeriodWait +
          ((i + 1) % maxPerPeriod) * intraPeriodWait;

        setTimeout(() => {
          console.info(
            new Date().toISOString(),
            'timeout',
            timeout,
            'Working on',
            ea,
            `(#${i + 1} / ${objs.length})`,
            'remaining timeout before next operation',
            i + 1 === objs.length ? 'none' : nextTimeout - timeout,
          );

          window.open(ea);

          if (i + 1 === objs.length) {
            console.log('finished processing all downloads');
          }
        }, timeout);
      }),
    initialWait,
  );
};

downloadAllSlowly();
