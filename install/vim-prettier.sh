#!/usr/bin/env bash

main() {
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"
  local YARN_BIN_PATH="${HOME}/.yarn/bin"

  local ROOT_DIR="${THIS_DIR}/.."

  local BASHRC_D="${ROOT_DIR}/.bashrc.d"
  local BIN_D="${ROOT_DIR}/bin"

  . "${HOME}/.bashrc"
  . "${BASHRC_D}/os"
  . "${HOME}/.nvm/nvm.sh"

  if [ ! -d "${HOME}/.vim/plugged/vim-prettier" ]; then
    vim -c "PlugInstall | qa"
  fi

  if macos; then
    YARN_BIN_PATH=/usr/local/bin

    export PATH="${YARN_BIN_PATH}:${PATH}"
  fi

  if ! [ -f "${YARN_BIN_PATH}/prettier" ]; then
    yarn global add prettier
  fi
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  main "$@"
fi
