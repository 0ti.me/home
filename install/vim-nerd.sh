#!/usr/bin/env bash

main() {
  local DEST=/tmp/vicc
  local FINAL_DEST=~/.vim
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"

  "${THIS_DIR}/../lib/install-vim-package-from-git-repo.sh" --repo=https://github.com/preservim/nerdcommenter
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  main "$@"
fi
