#!/usr/bin/env bash

main() {
  local BASHRC_D=
  local BIN_D=
  local ROOT_D=
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"


  ROOT_D="$(dirname "${THIS_DIR}")"
  BIN_D="${ROOT_D}/bin"
  BASHRC_D="${ROOT_D}/.bashrc.d"

  . "${BASHRC_D}/each"
  . "${BIN_D}/ierrcho"
  . "${BIN_D}/errcho"

  [ -d "${HOME}/.nvm" ] \
    || { errcho "Expected nvm to be installed" && return -1; }

  export NVM_DIR="${HOME}/.nvm"
  . "${NVM_DIR}/nvm.sh"

  NODE_V="${HOME}/.nvm/versions/node"

  for v in 6 7 8 9 10 11 12 14 16 18; do
    if [ ! -d "$(ls -1d "${NODE_V}/v${v}."* | head -n 1)" ]; then
      nvm install "${v}"
    fi
  done

  . "${BASHRC_D}/0001-nvm"

  npm install -g yarn
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  main "$@"
fi
