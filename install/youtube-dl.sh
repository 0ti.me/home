#!/usr/bin/env bash

main() {
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"
  local BIN_D="${THIS_DIR}/../bin"

  . "${BIN_D}/errcho"
  . "${BIN_D}/interactive"
  . "${BIN_D}/quiet-if-not"

  if which pamac >/dev/null; then
    pamac install --no-confirm --upgrade yt-dlp
    pamac install --no-confirm --as-deps atomicparsley aria2 python-mutagen python-pycryptodomex python-websockets

    return
  fi

  if which pipx >/dev/null 2>&1; then
    PIP=pipx
  elif which pip >/dev/null 2>&1; then
    PIP=pip
  elif which pip3 >/dev/null 2>&1; then
    PIP=pip3
  else
    errcho "no pip installed"
    return 1
  fi

  quiet-if-not interactive $PIP install --upgrade yt-dlp \
    || errcho "Failed to install yt-dlp"
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  main "$@"
fi
