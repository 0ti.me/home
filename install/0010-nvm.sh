#!/usr/bin/env bash

main() {
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"
  local BIN_D="${THIS_DIR}/../bin"

  . "${BIN_D}/ierrcho"
  . "${BIN_D}/interactive"
  . "${BIN_D}/quiet"
  . "${BIN_D}/quiet-if"
  . "${BIN_D}/quiet-if-not"

  set -e

  local DEST="${HOME}/.nvm"

  if ! [ -d "${DEST}" ]; then
    git clone https://github.com/nvm-sh/nvm "${DEST}"
  else
    quiet pushd "${DEST}"

    ierrcho -n "Pulling latest nvm: "

    quiet-if-not interactive git pull

    quiet popd
  fi
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  main "$@"
fi
