#!/usr/bin/env bash

noop() {
  return 0
}

main() {
  local DOCKER_COMPOSE="/usr/local/bin/docker-compose"
  local RELEASES_URL="https://api.github.com/repos/docker/compose/releases/latest"
  local VERSION=

  set -e

  if [ "${CAN_SUDO-1}" -eq 1 ]; then
    if which apt >/dev/null 2>/dev/null; then
      sudo apt remove docker-compose || noop
    fi

    VERSION=$(curl --silent "${RELEASES_URL}" | jq .name -r)

    sudo curl \
      -L \
      https://github.com/docker/compose/releases/download/${VERSION}/docker-compose-$(uname -s)-$(uname -m) \
      -o "${DOCKER_COMPOSE}" 2>/dev/null

    sudo chmod 755 "${DOCKER_COMPOSE}"
  fi
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  main "$@"
fi
