#!/usr/bin/env bash

main() {
  if which pamac >/dev/null; then
    pamac install --upgrade --no-confirm python-pip python-pipx

    return
  fi

  PYTHON=python

  if ! which python >/dev/null 2>&1 && which python3 >/dev/null 2>&1; then
    ln -s $(which python3) ~/.local/bin/python
    PYTHON=python3
  fi

  $PYTHON -m pip install --upgrade pip
}

main "$@"
