#!/usr/bin/env bash

neovim-0005() {
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"

  local NEOVIM_CONFIG_DIR="$(realpath "$(dirname "${THIS_DIR}")")"
  local OUTPUT_DIR="${HOME}/.local/bin"
  local OUTPUT_FILE="${OUTPUT_DIR}/nvim"
  local URL_NEOVIM_APPIMAGE="https://github.com/neovim/neovim/releases/latest/download/nvim.appimage"

  if [ "${NEOVIM_VERSION}" != "" ]; then
    URL_NEOVIM_APPIMAGE="https://github.com/neovim/neovim/releases/download/${NEOVIM_VERSION}/nvim.appimage"
  fi

  mkdir -p "${OUTPUT_DIR}"
  wget \
    --output-document="${OUTPUT_FILE}" \
    "${URL_NEOVIM_APPIMAGE}"
  chmod +x "${OUTPUT_FILE}"

  mkdir -p "${HOME}/.config"
  ensure-link "${NEOVIM_CONFIG_DIR}/.vim" "${HOME}/.config/nvim"

  # load up nvm to get a valid npm context for installing stuff
  . ~/.bashrc.d/0001-nvm

  "${OUTPUT_FILE}" --headless -c 'autocmd User PackerComplete quitall' -c 'PackerSync'
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  neovim-0005 "$@"
fi
