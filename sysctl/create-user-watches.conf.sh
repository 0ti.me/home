#!/usr/bin/env bash

main() {
  local BASHRC_D=
  local FILE=
  local SKIP_SET=0
  local SYSCTL_D=
  local SYSCTL_SET=
  local THIS="$(realpath "${BASH_SOURCE[0]:-${(%):-%x}}")"
  local THIS_DIR="$(dirname "${THIS}")"
  local USER_WATCHES_COUNT=${USER_WATCHES_COUNT:-65536}

  while [ $# -gt 0 ]; do
    case $1 in
      --skip-set)
        SKIP_SET=1
        ;;
      *)
        >&2 echo "$0: Unsupported arg $1"
        ;;
    esac

    shift
  done

  BASHRC_D="$(dirname "${THIS_DIR}")/.bashrc.d"

  . "${BASHRC_D}/os"

  if linux; then
    SYSCTL_D=/etc/sysctl.d
    SYSCTL_SET=( \
        sudo \
        sysctl \
        -w \
        fs.inotify.max_user_watches=${USER_WATCHES_COUNT} \
      )
  else
    >&2 echo "$0: Not implemented for current os"

    return 0
  fi

  FILE="${SYSCTL_D}/10-max-user-watches.conf"

  if [ ${SKIP_SET} -ne 1 ]; then
    eval "${SYSCTL_SET[@]}"
  fi

  echo "fs.inotify.max_user_watches=${USER_WATCHES_COUNT}" \
    | sudo tee "${FILE}"
}

if [[ "${BASH_SOURCE[0]:-${(%):-%x}}" == "$0" ]]; then
  main "$@"
fi
